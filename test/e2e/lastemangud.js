const config = require('../../nightwatch.conf.js');

module.exports = {
  Lastemangud(browser) {
    browser
      .url('https://lastemangud.ee')
      .waitForElementVisible('body')
      .assert.title('Arendavad mänguasjad | Frank & Artur | www.lastemängud.ee')
      .saveScreenshot(`${config.imgpath(browser)}FA-homepage.png`)
      .assert.elementPresent('#cookie-law-info-bar')
      .pause(10000)
      .assert.containsText('.cmsmasters_heading', 'Uued tooted')
      .assert.elementPresent('.cmsmasters_button', 'Kõik uued')
      .pause(500)
      .saveScreenshot(`${config.imgpath(browser)}FA-homepage-wo-cookies.png`)
      .useXpath()
      .click("//*[@id='cmsmasters_button_b9rfgvpe9']/a")
      .useCss()
      .waitForElementVisible('body')
      .assert.title('Uued tooted | Frank & Artur | www.lastemangud.ee')
      .saveScreenshot(`${config.imgpath(browser)}FA-Uued-tooted.png`)
      .useXpath()
      .click('//*[contains(text(), "Voolimismass “Moderna”")]')
      .useCss()
      .waitForElementVisible('body')
      .saveScreenshot(`${config.imgpath(browser)}FA-voolimismass.png`)
      .pause(500)
      .useXpath()
      .click('//*[@id="product-16940"]/div[1]/div[2]/form/button[text()="Lisa korvi"]')
      .useCss()
      .saveScreenshot(`${config.imgpath(browser)}FA-vaata-ostukorvi-nupp.png`)
      .pause(500)
      .useXpath()
      .click('//*[@id="middle"]/div[2]/div/div/div[1]/div/a[text()="Vaata ostukorvi"]')
      .pause(500)
      .useCss()
      .setValue('#coupon_code', 'franktoobära')
      .useXpath()
      .click('//*[@id="middle"]/div[2]/div/div/div[1]/form/table/tbody/tr[2]/td/div/button[text()="Kasuta sooduskoodi"]')
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}FA-ostukorv-sooduskupong.png`)
      .end();
  },
};
